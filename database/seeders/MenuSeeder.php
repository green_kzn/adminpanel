<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperadmin = Role::where('name', User::USER_ROLE_SUPERADMIN)->first();
        $roleAdmin = Role::where('name', User::USER_ROLES[1])->first();
        
        DB::table('menus')->insertGetId([
            'name' => 'Панель управления',
            'link' => '/home',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => 0,
        ]);
        $settings = DB::table('menus')->insertGetId([
            'name' => 'Настройки',
            'link' => '#',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => 0,
        ]);
        DB::table('menus')->insertGetId([
            'name' => 'Права',
            'link' => '/permissions',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => 0,
        ]);
        DB::table('menus')->insertGetId([
            'name' => 'Пользователи',
            'link' => '/users',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => $settings,
        ]);
        DB::table('menus')->insertGetId([
            'name' => 'Меню',
            'link' => '/menu',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => $settings,
        ]);
        DB::table('menus')->insertGetId([
            'name' => 'Роли',
            'link' => '/roles',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleSuperadmin->id,
            'parent_id' => $settings,
        ]);

        $settingsAdmin = DB::table('menus')->insertGetId([
            'name' => 'Настройки',
            'link' => '#',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleAdmin->id,
            'parent_id' => 0,
        ]);
        DB::table('menus')->insertGetId([
            'name' => 'Пользователи',
            'link' => '/users',
            'status' => Menu::STATUS_ENABLED,
            'role_id' => $roleAdmin->id,
            'parent_id' => $settingsAdmin,
        ]);
    }
}
