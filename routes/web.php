<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/login");
});

Auth::routes();

Route::group(['middleware' => ['role:Superadmin|Admin|Manager|Customer']], function () {
    Route::get('/home', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('home');

    Route::get('/users', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('users');

    Route::get('/getusers', [App\Http\Controllers\Superadmin\UserController::class, 'getUsers'])
        ->name('getusers');

    Route::get('/getuser/{id}', [App\Http\Controllers\Superadmin\UserController::class, 'getUser'])
        ->name('getuser');

    Route::get('/users/add', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('users.add');

    Route::post('/users/add', [App\Http\Controllers\Superadmin\UserController::class, 'postAdd'])
        ->name('users.save');

    Route::get('/users/view/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('users.view');

    Route::get('/users/edit/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('users.edit');

    Route::post('/users/edit', [App\Http\Controllers\Superadmin\UserController::class, 'postEdit'])
        ->name('users.saveedit');

    Route::get('/users/delete/{id}', [App\Http\Controllers\Superadmin\UserController::class, 'delete'])
        ->name('users.delete');

    Route::get('/menu', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('menu');

    Route::get('/getMenus', [App\Http\Controllers\Superadmin\MenuController::class, 'getAllMenuItems'])
        ->name('menu');

    Route::get('/getsubmenu', [App\Http\Controllers\Superadmin\MenuController::class, 'getSubmenu'])
        ->name('getsubmenu');

    Route::get('/getMenu/{id}', [App\Http\Controllers\Superadmin\MenuController::class, 'getMenuItem'])
        ->name('menu');

    Route::get('/menu/add', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('menu.add');

    Route::post('/menu/add', [App\Http\Controllers\Superadmin\MenuController::class, 'postAdd'])
        ->name('menu.save');

    Route::get('/menu/view/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('menu.view');

    Route::get('/menu/edit/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('menu.edit');

    Route::post('/menu/edit', [App\Http\Controllers\Superadmin\MenuController::class, 'postEdit'])
        ->name('menu.saveedit');

    Route::get('/menu/delete/{id}', [App\Http\Controllers\Superadmin\MenuController::class, 'delete'])
        ->name('menu.delete');

    Route::get('/roles', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('role');

    Route::get('/getroles', [App\Http\Controllers\Superadmin\RoleController::class, 'getRoles'])
        ->name('getroles');

    Route::get('/getrole/{id}', [App\Http\Controllers\Superadmin\RoleController::class, 'getRole'])
        ->name('getrole');

    Route::post('/getuserrole', [App\Http\Controllers\Superadmin\RoleController::class, 'getUserRole'])
        ->name('getuserrole');

    Route::get('/roles/add', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('role.add');

    Route::post('/roles/add', [App\Http\Controllers\Superadmin\RoleController::class, 'postAdd'])
        ->name('role.save');

    Route::get('/roles/view/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('role.view');

    Route::get('/roles/edit/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('role.edit');

    Route::post('/roles/edit', [App\Http\Controllers\Superadmin\RoleController::class, 'postEdit'])
        ->name('role.saveedit');

    Route::get('/roles/delete/{id}', [App\Http\Controllers\Superadmin\RoleController::class, 'delete'])
        ->name('role.delete');

    Route::resource('permissions', App\Http\Controllers\Superadmin\PermissionController::class);

    Route::get('/getuserpermission/{id}', [App\Http\Controllers\Superadmin\PermissionController::class, 'getUserPermission'])
        ->name('getuserpermission');

    Route::get('/permissions/user/edit/{id}', [App\Http\Controllers\Superadmin\HomeController::class, 'index'])
        ->name('permissionsuser');

    Route::get('/getuserpermission', [App\Http\Controllers\Superadmin\PermissionController::class, 'getPermission'])
        ->name('getuserpermission');
});
