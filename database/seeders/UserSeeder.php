<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'superadmin',
            'email' => 'superadmin@site.ru',
            'password' => Hash::make('password'),
        ]);

        $role = Role::where('name', User::USER_ROLE_SUPERADMIN)->first();
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole(User::USER_ROLE_SUPERADMIN);

        $userAdmin = User::create([
            'name' => 'admin',
            'email' => 'admin@site.ru',
            'password' => Hash::make('password'),
        ]);

        $role = Role::where('name', User::USER_ROLE_ADMIN)->first();
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $userAdmin->assignRole(User::USER_ROLE_ADMIN);
    }
}
