require ('./bootstrap.js');
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
// Vue.use(VueAxios, axios)
import { Form, HasError, AlertError } from 'vform';
window.Form = Form;

import Swal from 'sweetalert2';


const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
window.Swal = Swal;
window.Toast = Toast;

import App from './components/App'

import User from './components/Users/Index'

import Menu from './components/Menu/Index'
import MenuAdd from './components/Menu/Add'
import MenuEdit from './components/Menu/Edit'

import Role from './components/Role/Index'
import RoleAdd from './components/Role/Add'
import RoleView from './components/Role/View'
import Home from './components/Home'

import Permissions from './components/Permissions/Index'

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/home', name: 'home',  component: Home },
    { path: '/users', name: 'user', component: User },
    { path: '/menu', name: 'menu', component: Menu },
    { path: '/menu/add', name: 'menuadd', component: MenuAdd },
    { path: '/menu/edit/:id', name: 'menuedit', component: MenuEdit},
    { path: '/roles', name: 'role', component: Role },
    { path: '/roles/add', name: 'rolesadd', component: RoleAdd },
    { path: '/roles/view/:id', name: 'rolesview', component: RoleView },
    { path: '/permissions', name: 'permissions', component: Permissions },
  ]
});

const app = new Vue({
  el: '#app',
  components: { App },
  router,
});
